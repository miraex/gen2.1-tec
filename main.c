/*
 * Copyright (c) 2015-2019, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */



#include <msp430.h> 
#include <stdint.h>

// Function declaration
void initPins(void);
void initGPIOs(void);
void Software_Trim();                       // Software Trim to get the best DCOFTRIM value
void ADC_Samp (void);
int  ADC_OUT (void);
void Error_Calc (void);
float NTC_lookup (float);

#define MCLK_FREQ_MHZ 4                     // MCLK = 8MHz
#define PWM_Freq_kHz 10
#define Dutycycle_start 0.5                 // Starting duty cycle 0.151

// Fast less stable
//#define Kp 0.50
//#define Ki 0.30
//#define Kd 0.00

// slow but stable
//#define Kp 0.12 // 0.06
//#define Ki 0.12 // 0.05
//#define Kd 0.00 // 0.00

// dev
#define Kp 0.12
#define Ki 0.05
#define Kd 0.00

// board 2: 0.6
// board 3: 0.6
// board 4: 0.6
// board 7: 0.7
// board 8: 0.6
// board 10: 0.6
#define VREF 0.6

// Variable declaration
float Target_Temp = 40;
unsigned int Speed_CL;                      // Control Loop Speed in kHz
volatile float ADC_NTC;
volatile float ADC_VCTRL;
volatile float ADC_VIN;
volatile float ADC_RESULT;
volatile float VOUT;
volatile float error, error_old, error_i, error_i_old, error_d;
volatile float dt;
volatile float u;
volatile float Dutycycle=0.1;
volatile unsigned int i;
volatile unsigned int ADC_Complete=1;
float RNTC[5]={ 12250, 7880, 4950, 1000};
float TNTC[5]={ 20, 30, 40, 50};
volatile float Vref_NTC;
volatile float Res_NTC;
volatile float Count_Value;

/**
 * main.c
 */
int main(void)
{

    WDTCTL = WDTPW | WDTHOLD;	            // stop watchdog timer

	// Pin initializations
	initPins();
	initGPIOs();
	PM5CTL0 &= ~ LOCKLPM5;
	__bis_SR_register(GIE);                 // Enable interrupts

	// Calculate Vref_NTC
    // Res_NTC=  NTC_lookup (Target_Temp);
    // Vref_NTC= 1.234 * Res_NTC/(Res_NTC +  R10);

    Vref_NTC = VREF;   // Vref_NTC=0.43045 for 40C; Vref_NTC=0.55252 for 30C; Vref_NTC=0.68745 for 20C (TE NTC)

    // Setting Clock System
	__bis_SR_register(SCG0);                // disable FLL
    CSCTL3 |= SELREF__REFOCLK;              // Set REFO as FLL reference source
    CSCTL1 = DCOFTRIMEN | DCOFTRIM0 | DCOFTRIM1 | DCORSEL_3;// DCOFTRIM=3, DCO Range = 8MHz
    CSCTL2 = FLLD_0 + 243;                  // DCODIV = 8MHz
    __delay_cycles(3);
    CSCTL4 = SELMS__DCOCLKDIV | SELA__REFOCLK; // set default REFO(~32768Hz) as ACLK source, ACLK = 32768Hz
    CSCTL5 |= DIVM_0 | DIVS_0;              // Set MCLK to 8MHz and SMCLK to 8MHz
    __bic_SR_register(SCG0);                // enable FLL
    Software_Trim();                        // Software Trim to get the best DCOFTRIM value

    // Setting TimerA1 for PWM output on TA1.1
    TA1CTL |= TASSEL_2 | MC_0 |ID_0 | TACLR | TAIE; // SCLK, Timer Off, Timer clk= 8MHZ/1 = 8MHz; Counter cleared; Enable TAIFG
    TA1CCTL1 = OUTMOD_7;                    // CCR1 reset/set
    TA1CCR0 = (8000/(PWM_Freq_kHz))-1;      // CCR0 value based on desired PWM freq
    TA1CCR1 = TA1CCR0 * Dutycycle_start;    // Set PWM start duty cycle
    Count_Value = TA1CCR0 * Dutycycle_start;
    TA1CTL |= MC_1;                         // Turn on timer in Up mode

    // Control Loop Initialization
    // Setting TimerA0 for Control Loop
    TA0CTL = TASSEL_2 | MC_0 |ID_0 | TACLR | TAIE; // SCLK, Timer Off, Timer clk = 8MHZ/1 = 8MHz; Counter cleared; Enable TAIFG

    // ADC Configuration
    SYSCFG2 |= ADCPCTL1 | ADCPCTL3 |ADCPCTL0 | ADCPCTL7; // Configure ADC A3, A6 and A7 pins as input enabled; ADC function turned on for channel 0 to enable ext. Vref for ADC
    ADCCTL0 &=~ ADCENC;                     // Turn off ADC first and then config
    ADCCTL0 |= ADCSHT_15 | ADCON;           // ADCON, S&H=16 ADC clks
    ADCCTL1 |= ADCSHS_0 | ADCSHP | ADCSSEL_2 | ADCDIV_0; // S&H Source = ADCCS bit. Pulse sample mode. ADC Clock = SMCLK, Clock divider=1
    ADCCTL2 |= ADCRES_1;                    // 10-bit conversion results
    ADCMCTL0 |= ADCSREF_7 | ADCINCH_7;      // A7 ADC input select; Vref=VEREF
    ADCIE |= ADCIE0;                        // Enable ADC conv complete interrupt
    ADCMEM0= 0;
    ADCIFG|= ADCIFG0;                       // Trigger ADC conversion complete

    while (1)
    {
        if (ADC_Complete==1)
        {
            ADC_Complete=0;
            ADCCTL0  |= ADCENC ;
            ADCCTL0  |=ADCSC;               // Enable ADC
        }
    }
	return 0;
}

void initPins(void)
{
    P1SEL0 |= BIT1 | BIT3| BIT4 | BIT6 | BIT7; // Pins P1.1, P1.3, P1.6, P1.7 connected to ADC, and P1.4 connected to VREF+
    P1SEL1 |= BIT1 | BIT3| BIT4 | BIT5 | BIT6 | BIT7; // Pins P1.1, P1.3, P1.6, P1.7 connected to ADC, P1.5 connected to TA1  and P1.4 connected to VREF+

 }

void initGPIOs(void)
{
    P1DIR |= BIT5 ;                            // Pin 1.5 connected to TA1.1 for PWM signal
    P2DIR |= BIT0 | BIT1; P2OUT &=~ BIT0 | BIT1; // Pin 2.0 and 2.1 set to output direction for Mode and En control
    P2OUT |= BIT0 | BIT1;
}

float NTC_lookup(float Target)
{
    volatile float R;
    volatile unsigned int j;
    for (j=0; j<50; j++)
    {
        if (TNTC[j] == Target_Temp)
        {
            R = RNTC[j];
            break;                          // stop loop if match
        }
        else if ((TNTC[j] > Target_Temp) && j > 0)
        {
            R= RNTC[j-1] + ((RNTC[j] - RNTC[j-1])/(TNTC[j]-TNTC[j-1])) * (Target - TNTC[j-1]);
            break;                          // stop loop if match
        }

    }
    return R;
}

// ***********************************
// Timer A1 interrupt handler
// ***********************************
#pragma vector = TIMER1_A1_VECTOR
__interrupt void Timer1_A1_ISR(void)
{
    switch(__even_in_range(TA1IV,TA1IV_TAIFG))
    {
         case TA1IV_TAIFG:
         TA1CTL |= MC_0;                    // Turn off timer
         TA1CCR1 = Count_Value;             // Set PWM duty cycle
         TA1CTL |= MC_1;                    // Turn on timer in Up mode
         break;
         default:
         break;
    }
}

// ***********************************
// ADC interrupt handler
// ***********************************
#pragma vector=ADC_VECTOR
__interrupt void ADC_ISR(void)
{
    switch(__even_in_range(ADCIV,ADCIV_ADCIFG))
    {
         case ADCIV_ADCIFG:
             TA0CTL |= MC_0 ;
             ADCCTL0 &=~ ADCENC;                       // Disable ADC
             dt= TA0R * 0.000125;                      // dt= timer count * time per count
             ADC_RESULT = ADCMEM0;                     // Read ADC memory
             TA0CTL |= MC_2 | TACLR ;                  // Continuous mode. Clear count value
             ADC_NTC = ((1.234 * ADC_RESULT) / 1023);
             ADCMCTL0 |= ADCSREF_1 | ADCINCH_7;        // A1 ADC input select (VIN)
             i = 0;

             error = Vref_NTC - ADC_NTC;
             error_i = error_i_old + dt * 0.001 * error;                     // Rectangular Integration
             error_d = (error - error_old)  / (dt * 0.001);              // derivation

             u = Kp * error + Ki * error_i + Kd * error_d;

             // Tested on board 6
             // Dutycycle   Vout (with load)
             // 10.0%       6.72V
             // 20.0%       6.72V
             // 30.0%       6.77V
             // 34.0%       5.65
             // 35.0%       5.36V
             // 40.0%       3.95V
             // 50.0%       1.25V

             // Board       u0
             // 4           0.2349 (probably not good)
             // 6           0.34

             // originally 0.2349
             const float u0 = 0.34;
             const float umax = 0.45; // -> 2.5V on board6
             const float umin = 0.25; // -> 8.3V on board6

             if ((u0 - u) > umax ) {             // Limit upper duty cycle to 30% as this corresponds to VCTRL= 1V ==> Vout=5V
                 Dutycycle = umax;
             } else if ((u0 - u) < umin) {    // Limit lower duty cycle to 15% as this corresponds to VCTRL= 0.5V ==> Vout=1.8V
                 Dutycycle = umin;
                 error_i = error_i_old;
             } else {
                 error_i_old = error_i;
                 Dutycycle = u0 - u;                      // VCTRL for 3.3Vout is 0.7754V --> Dutycycle is 23.49%
             }

             error_i = error_i_old;
             error_old = error;

             Count_Value = TA1CCR0 * Dutycycle;
             break;

        default:
            break;
    }
    ADC_Complete=1;
}




// ***********************************
// Function to trim clock
// ***********************************
void Software_Trim()
{
    unsigned int oldDcoTap = 0xffff;
    unsigned int newDcoTap = 0xffff;
    unsigned int newDcoDelta = 0xffff;
    unsigned int bestDcoDelta = 0xffff;
    unsigned int csCtl0Copy = 0;
    unsigned int csCtl1Copy = 0;
    unsigned int csCtl0Read = 0;
    unsigned int csCtl1Read = 0;
    unsigned int dcoFreqTrim = 3;
    unsigned char endLoop = 0;

    do
    {
        CSCTL0 = 0x100;                        // DCO Tap = 256
        do
        {
            CSCTL7 &= ~DCOFFG;                 // Clear DCO fault flag
        } while (CSCTL7 & DCOFFG);              // Test DCO fault flag

        __delay_cycles((unsigned int)3000 * MCLK_FREQ_MHZ);// Wait FLL lock status (FLLUNLOCK) to be stable
                                                           // Suggest to wait 24 cycles of divided FLL reference clock
        while ((CSCTL7 & (FLLUNLOCK0 | FLLUNLOCK1)) && ((CSCTL7 & DCOFFG) == 0));

        csCtl0Read = CSCTL0;                   // Read CSCTL0
        csCtl1Read = CSCTL1;                   // Read CSCTL1

        oldDcoTap = newDcoTap;                 // Record DCOTAP value of last time
        newDcoTap = csCtl0Read & 0x01ff;       // Get DCOTAP value of this time
        dcoFreqTrim = (csCtl1Read & 0x0070)>>4;// Get DCOFTRIM value

        if(newDcoTap < 256)                    // DCOTAP < 256
        {
            newDcoDelta = 256 - newDcoTap;     // Delta value between DCPTAP and 256
            if((oldDcoTap != 0xffff) && (oldDcoTap >= 256)) // DCOTAP cross 256
                endLoop = 1;                   // Stop while loop
            else
            {
                dcoFreqTrim--;
                CSCTL1 = (csCtl1Read & (~(DCOFTRIM0+DCOFTRIM1+DCOFTRIM2))) | (dcoFreqTrim<<4);
            }
        }
        else                                   // DCOTAP >= 256
        {
            newDcoDelta = newDcoTap - 256;     // Delta value between DCPTAP and 256
            if(oldDcoTap < 256)                // DCOTAP cross 256
                endLoop = 1;                   // Stop while loop
            else
            {
                dcoFreqTrim++;
                CSCTL1 = (csCtl1Read & (~(DCOFTRIM0+DCOFTRIM1+DCOFTRIM2))) | (dcoFreqTrim<<4);
            }
        }

        if(newDcoDelta < bestDcoDelta)         // Record DCOTAP closest to 256
        {
            csCtl0Copy = csCtl0Read;
            csCtl1Copy = csCtl1Read;
            bestDcoDelta = newDcoDelta;
        }

    } while(endLoop == 0);                      // Poll until endLoop == 1

    CSCTL0 = csCtl0Copy;                       // Reload locked DCOTAP
    CSCTL1 = csCtl1Copy;                       // Reload locked DCOFTRIM
    while (CSCTL7 & (FLLUNLOCK0 | FLLUNLOCK1)); // Poll until FLL is locked
}
